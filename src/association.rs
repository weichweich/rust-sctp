use std::net::IpAddr;

use crate::{
    listener::MAX_UDP_PACKET_SIZE,
    packet::{
        chunk::{parameter::SctpParameter, value::Init, SctpChunkValue},
        SctpPacket,
    },
    SctpChunk, SctpHeader,
};
use bytes::BytesMut;

pub use self::{info::SctpAssociationInfo, lower_socket::LowerSocket};

mod info;
mod lower_socket;

pub struct SctpAssociation {
    pub local: SctpAssociationInfo,
    pub remote: SctpAssociationInfo,
}

impl SctpAssociation {
    pub async fn accept<LS: LowerSocket>(
        local_address: LS::LowerAddress,
        lower_socket: LS,
        local_cfg: SctpAssociationInfo,
    ) -> Result<Self, String> {
        let (remote_addr, remote_port, init) =
            receive_init_chunk(&lower_socket, local_cfg.port).await?;

        let ass = SctpAssociation {
            local: local_cfg,
            remote: SctpAssociationInfo {
                port: remote_port,
                outbound_streams: init.number_streams_outbound,
                max_inbound_streams: init.number_streams_inbound,
                a_rwnd: init.a_rwnd,
                addresses: init
                    .parameter
                    .iter()
                    .filter_map(|parm| match parm {
                        SctpParameter::Ipv4Addr(addr) => Some(IpAddr::V4(addr.clone())),
                        SctpParameter::Ipv6Addr(addr) => Some(IpAddr::V6(addr.clone())),
                        _ => None,
                    })
                    .collect(),
            },
        };

        send_init_ack(&lower_socket, remote_addr, ass).await?;

        todo!()
    }
}

/// Receives a SCTP packet with destination port `port` and returns the
/// contained init chunk. If more than one chunk is received or the packed
/// didn't contain an init chunk, an error is returned.
async fn receive_init_chunk<LS: LowerSocket>(
    lower_socket: &LS,
    port: u16,
) -> Result<(LS::LowerAddress, u16, Init), String> {
    let mut buf = BytesMut::with_capacity(MAX_UDP_PACKET_SIZE);
    buf.resize(MAX_UDP_PACKET_SIZE, 0x00_u8);

    let (len, remote_address) = lower_socket
        .recv_from(&mut buf)
        .await
        .map_err(|_| "lower socket err")?;
    buf.truncate(len);
    let mut received_data = buf.freeze();

    let pkt_init = SctpPacket::new_from_bytes(&mut received_data)
        .map_err(|e| format!("couldn't parse packet: {}", e))?;

    if pkt_init.chunks.len() > 1 {
        log::warn!("More than one chunk in packet. Other chunks are discarded.")
    }

    if pkt_init.header.destination_port != port {
        return Err("unexpected sctp port".to_owned());
    }

    let mut pkt_init = pkt_init;
    let chunk = pkt_init
        .chunks
        .drain(..)
        .find_map(|c| match c.value {
            SctpChunkValue::Init(init) => Some(init),
            _ => None,
        })
        .ok_or("Packet contained no init chunk")?;

    Ok((remote_address, pkt_init.header.source_port, chunk))
}

async fn send_init_ack<LS: LowerSocket>(
    lower_socket: &LS,
    dst: LS::LowerAddress,
    association: SctpAssociation,
) -> Result<(), String> {
    let chunk: SctpChunk = SctpChunkValue::InitAck(Init {
        // TODO: set tag
        initiate_tage: 0,
        a_rwnd: association.local.a_rwnd,
        number_streams_outbound: association.local.outbound_streams,
        number_streams_inbound: association.local.max_inbound_streams,
        // TODO: choose tsn
        initial_tsn: 0,
        parameter: vec![],
        unrecognized_parameter: vec![],
    })
    .into();

    let pkt = SctpPacket {
        header: SctpHeader {
            source_port: association.local.port,
            destination_port: association.remote.port,
            // TODO: real value
            verification_tag: 0,
            // TODO: calculate checksome somewhere
            checksum: 0,
        },
        chunks: vec![chunk],
    };

    let mut buf = BytesMut::new();
    pkt.write_to(&mut buf)?;
    let buf = buf.freeze();
    lower_socket
        .send_to(&buf[..], dst)
        .await
        .map_err(|_| "couldn't send".to_string())?;

    Ok(())
}

#[cfg(test)]
pub mod mock {
    use super::LowerSocket;
    use async_trait::async_trait;

    pub struct TestSocket {
        pub payload: Option<(u64, Vec<u8>)>,
    }

    #[async_trait]
    impl LowerSocket for TestSocket {
        type LowerAddress = u64;

        async fn peek_from(&self, buf: &mut [u8]) -> std::io::Result<(usize, Self::LowerAddress)> {
            if let Some((src_addr, src_buf)) = &self.payload {
                let to_copy = src_buf.len().min(buf.len());
                buf[0..to_copy].copy_from_slice(&src_buf[0..to_copy]);
                Ok((to_copy, *src_addr))
            } else {
                Ok((0, 0))
            }
        }

        async fn recv_from(&self, buf: &mut [u8]) -> std::io::Result<(usize, Self::LowerAddress)> {
            let ret = self.peek_from(buf).await?;

            Ok(ret)
        }

        async fn send_to(&self, buf: &[u8], dst: Self::LowerAddress) -> std::io::Result<usize> {
            println!("Send: {:?}", buf);
            Ok(buf.len())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use super::mock::TestSocket;

    const PKT_INIT1: [u8; 40] =
        hex!["1388138800000000825d9c050100001a6fdde21800100000ffffffff6f616d178008000682c00000"];

    #[tokio::test]
    async fn test_init() {
        let test_sock = TestSocket {
            payload: Some((12u64, PKT_INIT1.to_vec())),
        };

        let info = SctpAssociationInfo {
            port: 5000,
            outbound_streams: 2,
            max_inbound_streams: 2,
            a_rwnd: 1024 * 1024 * 1024,
            addresses: vec![],
        };

        let _ass = SctpAssociation::accept(13u64, test_sock, info)
            .await
            .expect("Should connect!");
    }
}
