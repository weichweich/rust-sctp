use bytes::{Buf, BufMut, Bytes, BytesMut};
use num_enum::{IntoPrimitive, TryFromPrimitive};
use std::{convert::TryInto, usize};

use self::{data::Data, heartbeat::HeartbeatInfo, init::Init, sack::SAck};

use super::SCTP_PADDING;

mod data;
mod heartbeat;
mod init;
pub mod parameter;
mod sack;

pub mod value {
    pub use super::{data::Data, heartbeat::HeartbeatInfo, init::Init, sack::SAck};
}

const CHUNK_HEADER_SIZE: usize = 4;

#[derive(Debug, PartialEq, Eq)]
pub struct SctpChunk {
    pub flags: u8,
    pub value: SctpChunkValue,
}

impl SctpChunk {
    pub fn encoded_len(&self) -> usize {
        CHUNK_HEADER_SIZE + self.value.encoded_len()
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        let chunk_type = match &self.value {
            SctpChunkValue::Data(_) => SctpChunkType::Data,
            SctpChunkValue::Init(_) => SctpChunkType::Init,
            SctpChunkValue::InitAck(_) => SctpChunkType::InitAck,
            SctpChunkValue::SAck(_) => SctpChunkType::SAck,
            SctpChunkValue::Heartbeat(_) => SctpChunkType::Heartbeat,
            SctpChunkValue::HeartbeatAck(_) => SctpChunkType::HeartbeatAck,
            SctpChunkValue::Abort => SctpChunkType::Abort,
            SctpChunkValue::Shutdown(_) => SctpChunkType::Shutdown,
            SctpChunkValue::ShutdownAck => SctpChunkType::ShutdownAck,
            SctpChunkValue::Error => SctpChunkType::Error,
            SctpChunkValue::CookieEcho(_) => SctpChunkType::CookieEcho,
            SctpChunkValue::CookieAck => SctpChunkType::CookieAck,
            SctpChunkValue::Ecne => SctpChunkType::Ecne,
            SctpChunkValue::Cwr => SctpChunkType::Cwr,
            SctpChunkValue::ShutdownComplete => SctpChunkType::ShutdownComplete,
        };
        let chunk_type: u8 = chunk_type.into();

        buf.put_u8(chunk_type);
        // TODO: handle chunk types
        buf.put_u8(0_u8);
        buf.put_u16(
            self.value
                .encoded_len()
                .try_into()
                .map_err(|_| "Chunk content to big")?,
        );
        self.value.write_to(buf)?;

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < CHUNK_HEADER_SIZE {
            return Err("sctp chunk to small".to_owned());
        }

        let raw_value = buf.get_u8();
        let flags = buf.get_u8();
        let length = buf.get_u16() as usize;

        // The header is included in the length, but we already removed the header from
        // the buffer, so we need to add it to the length.
        if buf.len() + CHUNK_HEADER_SIZE < length {
            return Err(format!(
                "Invalid chunk length {}. Buffer only has {} bytes.",
                length,
                buf.len() + CHUNK_HEADER_SIZE
            ));
        }

        let mut chunk_content = buf.split_to(length - CHUNK_HEADER_SIZE);

        let value = match SctpChunkType::try_from_primitive(raw_value)
            .map_err(|_| "unknown chunk type".to_owned())?
        {
            SctpChunkType::Data => {
                let mut data = Data::new_from_bytes(&mut chunk_content)?;
                data.set_flags_from_u8(flags);
                SctpChunkValue::Data(data)
            }

            SctpChunkType::Init => SctpChunkValue::Init(Init::new_from_bytes(&mut chunk_content)?),
            SctpChunkType::InitAck => {
                SctpChunkValue::InitAck(Init::new_from_bytes(&mut chunk_content)?)
            }

            SctpChunkType::CookieEcho => SctpChunkValue::CookieEcho(chunk_content),
            SctpChunkType::CookieAck => SctpChunkValue::CookieAck,
            SctpChunkType::SAck => SctpChunkValue::SAck(SAck::new_from_bytes(&mut chunk_content)?),
            SctpChunkType::Heartbeat => {
                SctpChunkValue::Heartbeat(HeartbeatInfo::new_from_bytes(&mut chunk_content)?)
            }

            SctpChunkType::HeartbeatAck => SctpChunkValue::HeartbeatAck(chunk_content),
            SctpChunkType::Abort => SctpChunkValue::Abort,
            SctpChunkType::Shutdown => {
                if chunk_content.len() < 4 {
                    return Err("Shutdown chunk to small".to_owned());
                }
                SctpChunkValue::Shutdown(chunk_content.get_u32())
            }
            SctpChunkType::ShutdownAck => SctpChunkValue::ShutdownAck,
            SctpChunkType::Error => SctpChunkValue::Error,
            SctpChunkType::Ecne => SctpChunkValue::Ecne,
            SctpChunkType::Cwr => SctpChunkValue::Cwr,
            SctpChunkType::ShutdownComplete => SctpChunkValue::ShutdownComplete,
        };

        // sctp chunk data is padded to have a length of multiple of 4 bytes
        let padding: usize = crate::util::padding(length, SCTP_PADDING);
        buf.advance(padding.min(buf.len()));

        Ok(Self { flags, value })
    }
}

impl From<SctpChunkValue> for SctpChunk {
    fn from(value: SctpChunkValue) -> Self {
        SctpChunk { flags: 0, value }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum SctpChunkValue {
    Data(Data),
    Init(Init),
    InitAck(Init),
    SAck(SAck),
    Heartbeat(HeartbeatInfo),
    HeartbeatAck(Bytes),
    Abort,

    /// Signals the graceful close of an association. Contains a cumulative TSN
    /// Ack.
    Shutdown(u32),
    ShutdownAck,
    Error,
    CookieEcho(Bytes),
    CookieAck,
    Ecne,
    Cwr,
    ShutdownComplete,
}

impl SctpChunkValue {
    pub fn encoded_len(&self) -> usize {
        match self {
            SctpChunkValue::Data(d) => d.encoded_len(),
            SctpChunkValue::Init(i) => i.encoded_len(),
            SctpChunkValue::InitAck(i) => i.encoded_len(),
            SctpChunkValue::SAck(s) => s.encoded_len(),
            SctpChunkValue::Heartbeat(b) => b.encoded_len(),
            SctpChunkValue::HeartbeatAck(b) => b.len(),
            SctpChunkValue::Abort => todo!(),
            SctpChunkValue::Shutdown(_) => 4,
            SctpChunkValue::ShutdownAck => 0,
            SctpChunkValue::Error => todo!(),
            SctpChunkValue::CookieEcho(b) => b.len(),
            SctpChunkValue::CookieAck => 0,
            SctpChunkValue::Ecne => todo!(),
            SctpChunkValue::Cwr => todo!(),
            SctpChunkValue::ShutdownComplete => 0,
        }
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        match self {
            SctpChunkValue::Data(d) => d.write_to(buf)?,
            SctpChunkValue::Init(i) => i.write_to(buf)?,
            SctpChunkValue::InitAck(i) => i.write_to(buf)?,
            SctpChunkValue::SAck(s) => s.write_to(buf)?,
            SctpChunkValue::Heartbeat(b) => b.write_to(buf)?,
            SctpChunkValue::HeartbeatAck(b) => buf.put(&b[..]),
            SctpChunkValue::Abort => todo!(),
            SctpChunkValue::Shutdown(tsn) => buf.put_u32(*tsn),
            SctpChunkValue::ShutdownAck => {}
            SctpChunkValue::Error => todo!(),
            SctpChunkValue::CookieEcho(b) => buf.put(&b[..]),
            SctpChunkValue::CookieAck => {}
            SctpChunkValue::Ecne => todo!(),
            SctpChunkValue::Cwr => todo!(),
            SctpChunkValue::ShutdownComplete => {}
        };
        Ok(())
    }
}

#[derive(Debug, Eq, PartialEq, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum SctpChunkType {
    Data = 0,
    Init = 1,
    InitAck = 2,
    SAck = 3,
    Heartbeat = 4,
    HeartbeatAck = 5,
    Abort = 6,
    Shutdown = 7,
    ShutdownAck = 8,
    Error = 9,
    CookieEcho = 10,
    CookieAck = 11,

    /// Reserved for Explicit Congestion Notification Echo
    Ecne = 12,

    Cwr = 13,
    ShutdownComplete = 14,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_serialize_type() {
        assert_eq!(
            Ok(SctpChunkType::Data),
            SctpChunkType::try_from_primitive(0u8)
        );
        assert_eq!(
            Ok(SctpChunkType::ShutdownComplete),
            SctpChunkType::try_from_primitive(14u8)
        );
    }
}
