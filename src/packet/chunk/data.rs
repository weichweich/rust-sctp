/// ! Encode and Decode  tsn: (), stream_id: (), sequence_num: (), protocol_identifier: (), data: (), unordered: (), beginning_fragment: (), ending_fragment: () tsn: (), stream_id: (), sequence_num: (), protocol_identifier: (), data: (), unordered: (), beginning_fragment: (), ending_fragment: ()Payload Data Chunks. https://tools.ietf.org/html/rfc4960#section-3.3.1
use bytes::{Buf, BufMut, Bytes, BytesMut};

const SCTP_DATA_HEADER_SIZE: usize = 12;

const UNORDERED_BIT: u8 = 1;
const BEGINNING_BIT: u8 = 1_u8.wrapping_shl(1);
const ENDING_BIT: u8 = 1_u8.wrapping_shl(2);

#[derive(Debug, PartialEq, Eq)]
pub struct Data {
    pub tsn: u32,
    pub stream_id: u16,
    pub sequence_num: u16,
    pub protocol_identifier: u32,
    pub data: Bytes,

    pub unordered: bool,
    pub beginning_fragment: bool,
    pub ending_fragment: bool,
}

impl Data {
    pub fn encoded_len(&self) -> usize {
        SCTP_DATA_HEADER_SIZE + self.data.len()
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        buf.put_u32(self.tsn);
        buf.put_u16(self.stream_id);
        buf.put_u16(self.sequence_num);
        buf.put_u32(self.protocol_identifier);
        buf.put(&self.data[..]);

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < SCTP_DATA_HEADER_SIZE {
            return Err(format!(
                "Data header to small, expected {} but got {} bytes.",
                SCTP_DATA_HEADER_SIZE,
                buf.len()
            ));
        }
        let tsn = buf.get_u32();
        let stream_id = buf.get_u16();
        let sequence_num = buf.get_u16();
        let protocol_identifier = buf.get_u32();

        Ok(Self {
            tsn,
            stream_id,
            sequence_num,
            protocol_identifier,
            data: buf.split_to(buf.len()),
            unordered: false,
            beginning_fragment: false,
            ending_fragment: false,
        })
    }

    pub fn set_flags_from_u8(&mut self, flags: u8) {
        self.unordered = flags & UNORDERED_BIT == UNORDERED_BIT;
        self.beginning_fragment = flags & BEGINNING_BIT == BEGINNING_BIT;
        self.ending_fragment = flags & ENDING_BIT == ENDING_BIT;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const PKT_DATA: [u8; 20] = hex!["9c7a095a000000000000003370696e6720300000"];

    #[test]
    fn test_decode() {
        let mut data_buf = Bytes::copy_from_slice(&PKT_DATA[..]);
        let payload = data_buf.slice(SCTP_DATA_HEADER_SIZE..);

        let decoded = Data::new_from_bytes(&mut data_buf).expect("Should decode data chunk");

        assert_eq!(data_buf.len(), 0, "The whole buffer should be consumed");

        assert_eq!(
            decoded,
            Data {
                tsn: 2625243482,
                stream_id: 0,
                sequence_num: 0,
                protocol_identifier: 51,
                data: payload,
                unordered: false,
                beginning_fragment: false,
                ending_fragment: false,
            }
        )
    }

    #[test]
    fn test_decode_empty() {
        let mut data_buf = Bytes::copy_from_slice(&PKT_DATA[0..0]);
        assert!(Data::new_from_bytes(&mut data_buf).is_err());
    }

    #[test]
    fn test_encode() {
        let payload = Bytes::copy_from_slice(&PKT_DATA[SCTP_DATA_HEADER_SIZE..]);
        let data = Data {
            tsn: 2625243482,
            stream_id: 0,
            sequence_num: 0,
            protocol_identifier: 51,
            data: payload,
            unordered: false,
            beginning_fragment: false,
            ending_fragment: false,
        };

        let mut data_buf = BytesMut::new();
        data.write_to(&mut data_buf)
            .expect("Should successfully encode");

        assert_eq!(&data_buf[..], &PKT_DATA[..]);
    }

    #[test]
    fn test_encode_empty() {
        let payload = Bytes::copy_from_slice(&PKT_DATA[0..0]);
        let data = Data {
            tsn: 2625243482,
            stream_id: 0,
            sequence_num: 0,
            protocol_identifier: 51,
            data: payload,
            unordered: false,
            beginning_fragment: false,
            ending_fragment: false,
        };

        let mut data_buf = BytesMut::new();
        data.write_to(&mut data_buf)
            .expect("Should successfully encode");

        assert_eq!(&data_buf[..], &PKT_DATA[..SCTP_DATA_HEADER_SIZE]);
    }
}
