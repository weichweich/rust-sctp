use bytes::{Buf, BufMut, Bytes, BytesMut};

use super::parameter::{parameter_from_bytes, SctpParameter, SctpUnrecognizedParameter};

const INIT_CHUNK_HEADER_SIZE: usize = 16;

#[derive(Debug, PartialEq, Eq)]
pub struct Init {
    pub initiate_tage: u32,
    pub a_rwnd: u32,
    pub number_streams_outbound: u16,
    pub number_streams_inbound: u16,
    pub initial_tsn: u32,
    pub parameter: Vec<SctpParameter>,

    /// Parameters that are not known by this implementation and must be
    /// reported to the sender of the init chunk.
    ///
    /// NOTE: Only parameter that are required to be reported are listed here.
    /// Even if the vec is empty, parameters might not be recognized.
    pub unrecognized_parameter: Vec<SctpUnrecognizedParameter>,
}

impl Init {
    pub fn encoded_len(&self) -> usize {
        let parameter_length = self
            .parameter
            .iter()
            .fold(0, |acc, p| acc + p.encoded_len());
        let unknwon_parameter_length = self
            .unrecognized_parameter
            .iter()
            .fold(0, |acc, p| acc + p.encoded_len());

        INIT_CHUNK_HEADER_SIZE + parameter_length + unknwon_parameter_length
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        buf.put_u32(self.initiate_tage);
        buf.put_u32(self.a_rwnd);
        buf.put_u16(self.number_streams_outbound);
        buf.put_u16(self.number_streams_inbound);
        buf.put_u32(self.initial_tsn);

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < INIT_CHUNK_HEADER_SIZE {
            return Err("sctp chunk value to small for init chunk".to_owned());
        }
        let initiate_tage = buf.get_u32();
        let a_rwnd = buf.get_u32();
        let number_streams_outbound = buf.get_u16();
        let number_streams_inbound = buf.get_u16();
        let initial_tsn = buf.get_u32();

        let (parameter, unrecognized_parameter) = parameter_from_bytes(buf)?;

        Ok(Self {
            initiate_tage,
            a_rwnd,
            number_streams_outbound,
            number_streams_inbound,
            initial_tsn,
            parameter,
            unrecognized_parameter,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const CHUNK_VALUE_INIT: [u8; 24] = hex!["6fdde21800100000ffffffff6f616d178008000682c00000"];
    const CHUNK_VALUE_INIT_INVALID: [u8; 13] = hex!["00000000b0673b940100001a70"];

    #[test]
    pub fn test_serialize_init_chunk() {
        let chunk = Init::new_from_bytes(&mut Bytes::copy_from_slice(&CHUNK_VALUE_INIT[..]))
            .expect("should parse CHUNK_VALUE_INIT");
        assert_eq!(
            Init {
                initiate_tage: 0x6fdde218,
                a_rwnd: 1048576,
                number_streams_outbound: 65535,
                number_streams_inbound: 65535,
                initial_tsn: 1868655895,
                parameter: vec![],
                unrecognized_parameter: vec![],
            },
            chunk
        );
        println!("Init chunk: {:?}", chunk);
    }

    #[test]
    pub fn test_serialize_init_chunk_short() {
        let chunk =
            Init::new_from_bytes(&mut Bytes::copy_from_slice(&CHUNK_VALUE_INIT_INVALID[..]));
        assert_eq!(
            Err("sctp chunk value to small for init chunk".to_owned()),
            chunk
        );
        println!("Init chunk: {:?}", chunk);
    }
}
