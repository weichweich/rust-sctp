use bytes::{Buf, BufMut, Bytes, BytesMut};
use std::convert::TryFrom;

const SCTP_HEARTBEAT_HEADER_SIZE: usize = 4;

#[derive(Debug, PartialEq, Eq)]
pub struct HeartbeatInfo {
    info_type: u16,
    sender_specific_info: Bytes,
}

impl HeartbeatInfo {
    pub fn encoded_len(&self) -> usize {
        SCTP_HEARTBEAT_HEADER_SIZE + self.sender_specific_info.len()
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        buf.put_u16(self.info_type);
        buf.put_u16(u16::try_from(self.sender_specific_info.len()).map_err(|_| {
            format!(
                "sender specific info exceedes length field (max {}).",
                u16::MAX
            )
        })?);
        buf.copy_from_slice(&self.sender_specific_info[..]);

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < SCTP_HEARTBEAT_HEADER_SIZE {
            return Err(format!(
                "Heartbeat header to small, expected {} but got {} bytes.",
                SCTP_HEARTBEAT_HEADER_SIZE,
                buf.len()
            ));
        }
        let info_type = buf.get_u16();
        let info_length = buf.get_u16() as usize;

        if buf.len() + SCTP_HEARTBEAT_HEADER_SIZE < info_length {
            return Err(format!(
                "Heartbeat header to small, expected {} but got {} bytes.",
                SCTP_HEARTBEAT_HEADER_SIZE,
                buf.len()
            ));
        }
        Ok(Self {
            info_type,
            sender_specific_info: buf.split_to(info_length - SCTP_HEARTBEAT_HEADER_SIZE),
        })
    }
}
