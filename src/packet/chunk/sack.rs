use bytes::{Buf, BufMut, Bytes, BytesMut};
use std::convert::TryFrom;

const SCTP_SACK_HEADER_SIZE: usize = 12;

/// Selective acknowledgement
///
/// https://tools.ietf.org/html/rfc4960#section-3.3.4
#[derive(Debug, PartialEq, Eq)]
pub struct SAck {
    /// Cumulative TSN Ack
    tsn_ack: u32,

    /// Advertised receiver Window Credit
    a_rwnd: u32,

    gaps_ack: Vec<(u16, u16)>,

    duplicate_tsns: Vec<u32>,
}

impl SAck {
    pub fn encoded_len(&self) -> usize {
        SCTP_SACK_HEADER_SIZE + self.gaps_ack.len() * 4 + self.duplicate_tsns.len() * 4
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        buf.put_u32(self.tsn_ack);
        buf.put_u32(self.a_rwnd);

        buf.put_u16(
            u16::try_from(self.gaps_ack.len()).map_err(|_| "To many gap ack blocks.".to_owned())?,
        );

        buf.put_u16(
            u16::try_from(self.gaps_ack.len())
                .map_err(|_| "To many duplicate TSN entries.".to_owned())?,
        );

        for (start, end) in self.gaps_ack.iter() {
            buf.put_u16(*start);
            buf.put_u16(*end);
        }

        for dup_tsn in self.duplicate_tsns.iter() {
            buf.put_u32(*dup_tsn);
        }

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < SCTP_SACK_HEADER_SIZE {
            return Err(format!(
                "SAck header to small, expected {} but got {} bytes.",
                SCTP_SACK_HEADER_SIZE,
                buf.len()
            ));
        }

        let tsn_ack = buf.get_u32();
        let a_rwnd = buf.get_u32();

        let gaps_number = buf.get_u16() as usize;
        let tsn_number = buf.get_u16() as usize;

        if buf.len() < gaps_number * 4 + tsn_number * 4 {
            return Err(format!(
                "SAck body to small, expected {} but got {} bytes.",
                gaps_number * 4 + tsn_number * 4,
                buf.len()
            ));
        }

        let mut gaps_ack = Vec::with_capacity(gaps_number);
        for _ in 0..gaps_number {
            // (start offset TSN, end offset TSN)
            gaps_ack.push((buf.get_u16(), buf.get_u16()));
        }

        let mut duplicate_tsns = Vec::with_capacity(tsn_number);
        for _ in 0..gaps_number {
            // (start offset TSN, end offset TSN)
            duplicate_tsns.push(buf.get_u32());
        }

        Ok(Self {
            tsn_ack,
            a_rwnd,
            gaps_ack,
            duplicate_tsns,
        })
    }
}
