use std::{
    convert::{TryFrom, TryInto},
    net::{Ipv4Addr, Ipv6Addr},
};

use bytes::{Buf, Bytes, BytesMut};
use num_enum::{IntoPrimitive, TryFromPrimitive};

use crate::util::padding;

const PARAMETER_HEADER_LENGTH: usize = 4;
const PARAMETER_PADDING: usize = 4;

#[derive(Debug, Eq, PartialEq, TryFromPrimitive, IntoPrimitive)]
#[repr(u16)]
enum SctpParameterType {
    // HeartbeatInfo = 1,
    Ipv4Address = 5,
    Ipv6Address = 6,
    StateCookie = 7,
    UnrecognizedParameter = 8,
    /* CookiePreservative = 9,
     * Hostname = 11,
     * SupportedAddressTypes = 12,
     * OutSsnResetReq = 13,
     * IncSsnResetReq = 14,
     * SsnTsnResetReq = 15,
     * ReconfigResp = 16,
     * AddOutStreamsReq = 17,
     * AddIncStreamsReq = 18,
     * EcnCapable = 32768,
     * Random = 32770,
     * ChunkList = 32771,
     * ReqHmacAlgo = 32772,
     * Padding = 32773,
     * SupportedExt = 32776,
     * ForwardTSNSupp = 49152,
     * AddIpAddr = 49153,
     * DelIpAddr = 49154,
     * ErrClauseInd = 49155,
     * SetPriAddr = 49156,
     * SuccessInd = 49157,
     * AdaptLayerInd = 49158, */
}

impl SctpParameterType {
    fn unsupported_type_action(value: u16) -> Action {
        match value & 0b1100_0000_0000_0000 {
            0b0000_0000_0000_0000 => Action::StopProcessing,
            0b0100_0000_0000_0000 => Action::StopProcessingReport,
            0b1000_0000_0000_0000 => Action::Skip,
            0b1100_0000_0000_0000 => Action::SkipReport,

            _ => unreachable!(
                "The matched value was masked. Only the 2 most significant bit can be 1."
            ),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Action {
    StopProcessing,
    StopProcessingReport,
    Skip,
    SkipReport,
}

pub enum DecodeError {
    Fatal(String),
    UnsupportedParameter(u16, Action),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SctpParameter {
    Ipv4Addr(Ipv4Addr),
    Ipv6Addr(Ipv6Addr),
    StateCookie(Bytes),
    UnrecognizedParameter(Vec<SctpUnrecognizedParameter>),
}

impl SctpParameter {
    /// The length of the parameter without the parameter header.
    pub fn encoded_len(&self) -> usize {
        let content_len = match self {
            SctpParameter::Ipv4Addr(_) => 4,
            SctpParameter::Ipv6Addr(_) => 16,
            SctpParameter::StateCookie(buf) => buf.len(),
            SctpParameter::UnrecognizedParameter(p) => p.len(),
        };
        content_len + PARAMETER_HEADER_LENGTH
    }

    pub fn write_to(&self, dst: &mut BytesMut) -> Result<(), String> {
        let length = self.encoded_len();
        let padding = padding(length, PARAMETER_PADDING);

        dst.reserve(self.encoded_len() + padding);

        // Write Type
        dst.extend_from_slice(&u16::from(self.get_type()).to_be_bytes()[..]);

        // Write Length
        let length: u16 = (length + padding)
            .try_into()
            .map_err(|_| "parameter size exceeds length field".to_string())?;
        dst.extend_from_slice(&length.to_be_bytes());

        // Write Value
        match self {
            SctpParameter::Ipv4Addr(addr) => dst.extend_from_slice(&addr.octets()[..]),
            SctpParameter::Ipv6Addr(addr) => dst.extend_from_slice(&addr.octets()[..]),
            SctpParameter::StateCookie(buf) => dst.extend_from_slice(&buf[..]),
            SctpParameter::UnrecognizedParameter(param) => {
                param.iter().try_for_each(|p| p.write_to(dst))?
            }
        };

        // Write Padding
        dst.extend(std::iter::repeat(0u8).take(padding));

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, DecodeError> {
        // peek the parameter type
        let raw_type = buf.clone().get_u16();
        let param_type = SctpParameterType::try_from_primitive(raw_type).map_err(|err| {
            DecodeError::UnsupportedParameter(
                err.number,
                SctpParameterType::unsupported_type_action(err.number),
            )
        })?;

        // only if we support the type, we consume the parameter type
        buf.get_u16();

        // length of type, length and value fields
        let length = buf.get_u16() as usize;

        if buf.len() + PARAMETER_HEADER_LENGTH < length {
            return Err(DecodeError::Fatal(format!(
                "invalid parameter length! Buffer has size {}, but length is {}",
                buf.len() + PARAMETER_HEADER_LENGTH,
                length
            )));
        }
        let param_value = buf.split_to(length - PARAMETER_HEADER_LENGTH);

        let param = match param_type {
            SctpParameterType::Ipv4Address => {
                let raw_addr: [u8; 4] = param_value[..4]
                    .try_into()
                    .expect("The buffers size was checked. □");
                SctpParameter::Ipv4Addr(Ipv4Addr::from(raw_addr))
            }

            SctpParameterType::Ipv6Address => {
                let raw_addr: [u8; 16] = param_value[..16]
                    .try_into()
                    .expect("The buffers size was checked. □");
                SctpParameter::Ipv6Addr(Ipv6Addr::from(raw_addr))
            }

            SctpParameterType::StateCookie => SctpParameter::StateCookie(param_value),

            SctpParameterType::UnrecognizedParameter => {
                todo!()
            }
        };

        let padding = padding(length, PARAMETER_PADDING);
        buf.advance(padding.min(buf.len()));

        Ok(param)
    }

    fn get_type(&self) -> SctpParameterType {
        match self {
            SctpParameter::Ipv4Addr(_) => SctpParameterType::Ipv4Address,
            SctpParameter::Ipv6Addr(_) => SctpParameterType::Ipv6Address,
            SctpParameter::StateCookie(_) => SctpParameterType::StateCookie,
            SctpParameter::UnrecognizedParameter(_) => SctpParameterType::UnrecognizedParameter,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SctpUnrecognizedParameter {
    parameter_type: u16,
    value: Bytes,
}

impl SctpUnrecognizedParameter {
    pub fn encoded_len(&self) -> usize {
        self.value.len() + PARAMETER_HEADER_LENGTH
    }

    pub fn write_to(&self, dst: &mut BytesMut) -> Result<(), String> {
        dst.reserve(self.encoded_len());

        dst.extend_from_slice(&self.parameter_type.to_be_bytes()[..]);

        let length = u16::try_from(self.encoded_len())
            .map_err(|_| "Unrecognized parameter size exceeds length field")?;
        dst.extend_from_slice(&length.to_be_bytes()[..]);

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        let raw_type = buf.get_u16();

        // length of type, length and value fields
        let length = buf.get_u16() as usize;

        if buf.len() + PARAMETER_HEADER_LENGTH < length {
            return Err(format!(
                "invalid parameter length! Buffer has size {}, but length is {}",
                buf.len() + PARAMETER_HEADER_LENGTH,
                length
            ));
        }
        let param_value = buf.split_to(length - PARAMETER_HEADER_LENGTH);

        Ok(Self {
            parameter_type: raw_type,
            value: param_value,
        })
    }
}

// TODO: make me pretty!
pub(super) fn parameter_from_bytes(
    mut parameter_buf: &mut Bytes,
) -> Result<(Vec<SctpParameter>, Vec<SctpUnrecognizedParameter>), String> {
    let mut parameter = Vec::new();
    let mut unrecognized_param = Vec::new();

    while parameter_buf.len() > PARAMETER_HEADER_LENGTH {
        let last_len = parameter_buf.len();

        match SctpParameter::new_from_bytes(&mut parameter_buf) {
            Ok(param) => parameter.push(param),

            Err(DecodeError::UnsupportedParameter(_, Action::StopProcessing)) => {
                return Ok((parameter, unrecognized_param));
            }

            Err(DecodeError::UnsupportedParameter(type_num, Action::StopProcessingReport)) => {
                log::info!(
                    "Unsupported parameter ({}). Report and stop processing parameter.",
                    type_num
                );
                unrecognized_param.push(SctpUnrecognizedParameter::new_from_bytes(
                    &mut parameter_buf,
                )?);
                return Ok((parameter, unrecognized_param));
            }

            Err(DecodeError::UnsupportedParameter(type_num, Action::Skip)) => {
                log::info!("skip unsupported parameter. ({})", type_num);
                // parse parameter, but discard. The pointe into parameter_buf will advance.
                SctpUnrecognizedParameter::new_from_bytes(&mut parameter_buf)?;
            }

            Err(DecodeError::UnsupportedParameter(type_num, Action::SkipReport)) => {
                unrecognized_param.push(SctpUnrecognizedParameter::new_from_bytes(
                    &mut parameter_buf,
                )?);
                log::info!("report and skip unsupported parameter ({}).", type_num)
            }

            Err(DecodeError::Fatal(msg)) => return Err(msg),
        }

        assert!(
            last_len > parameter_buf.len(),
            "the buffer must get smaller with each loop, or an error has to be returned"
        );
    }

    Ok((parameter, unrecognized_param))
}

#[cfg(test)]
mod tests {
    use super::*;
    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    const IPV4_PARAMETER_RAW: [u8; 8] = hex!["000500087f000001"];
    const IPV6_PARAMETER_RAW: [u8; 20] = hex!["000600147f0000017f0000017f0000017f000001"];

    #[test]
    fn test_unsupported_type() {
        init();
        assert_eq!(
            SctpParameterType::unsupported_type_action(0b0000_0000_0000_0000),
            Action::StopProcessing
        );
        assert_eq!(
            SctpParameterType::unsupported_type_action(0b0100_0000_0000_0000),
            Action::StopProcessingReport
        );
        assert_eq!(
            SctpParameterType::unsupported_type_action(0b1000_0000_0000_0000),
            Action::Skip
        );
        assert_eq!(
            SctpParameterType::unsupported_type_action(0b1100_0000_0000_0000),
            Action::SkipReport
        );

        // check that the 14 least significant bits don't matter
        for i in 0..14 {
            assert_eq!(
                SctpParameterType::unsupported_type_action(
                    0b0000_0000_0000_0001_u16.wrapping_shl(i)
                ),
                Action::StopProcessing,
                "Should ignore the {}th least significant bit.",
                i
            );
        }
    }

    #[test]
    fn encode_decode() {
        init();
        let mut dst = BytesMut::new();

        let param_v4 = SctpParameter::Ipv4Addr(Ipv4Addr::from([127, 0, 0, 1]));
        assert!(param_v4.write_to(&mut dst).is_ok());
        assert_eq!(0, dst.len() % PARAMETER_PADDING);

        let param_v6 = SctpParameter::Ipv6Addr(Ipv6Addr::from([
            127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1,
        ]));
        assert!(param_v6.write_to(&mut dst).is_ok());
        assert_eq!(0, dst.len() % PARAMETER_PADDING);

        let decoded_param = parameter_from_bytes(&mut Bytes::from(dst))
            .expect("Encoded parameter must always be able to be decoded");
        assert_eq!((vec![param_v4, param_v6], vec![]), decoded_param);
    }

    #[test]
    fn encode_ipv4() {
        init();
        let mut dst = BytesMut::new();

        let param_v4 = SctpParameter::Ipv4Addr(Ipv4Addr::from([127, 0, 0, 1]));
        assert!(param_v4.write_to(&mut dst).is_ok());
        assert_eq!(&dst.freeze()[..], &IPV4_PARAMETER_RAW[..]);
    }

    #[test]
    fn encode_ipv6() {
        init();
        let mut dst = BytesMut::new();

        let param_v6 = SctpParameter::Ipv6Addr(Ipv6Addr::from([
            127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1,
        ]));
        assert!(param_v6.write_to(&mut dst).is_ok());
        assert_eq!(&dst.freeze()[..], &IPV6_PARAMETER_RAW[..]);
    }

    #[test]
    fn decode_ipv4() {
        init();
        let param_v4 = SctpParameter::Ipv4Addr(Ipv4Addr::from([127, 0, 0, 1]));

        assert_eq!(
            vec![param_v4],
            parameter_from_bytes(&mut Bytes::copy_from_slice(&IPV4_PARAMETER_RAW[..]))
                .expect("Should decode ipv4 parameter")
                .0
        );
    }

    #[test]
    fn decode_ipv6() {
        init();
        let param_v6 = SctpParameter::Ipv6Addr(Ipv6Addr::from([
            127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1, 127, 0, 0, 1,
        ]));

        assert_eq!(
            vec![param_v6],
            parameter_from_bytes(&mut Bytes::copy_from_slice(&IPV6_PARAMETER_RAW[..]))
                .expect("Should decode ipv4 parameter")
                .0
        );
    }
}
