use std::usize;

use bytes::{Buf, BufMut, Bytes, BytesMut};

use crate::packet::chunk::SctpChunk;

pub mod chunk;

/// The size of a sctp header in bytes.
const SCTP_HEADER_SIZE: usize = 12;
const SCTP_PADDING: usize = 4;

pub struct SctpPacket {
    pub header: SctpHeader,
    pub chunks: Vec<SctpChunk>,
}

impl SctpPacket {
    pub fn encoded_len(&self) -> usize {
        SCTP_HEADER_SIZE + self.chunks.iter().fold(0, |acc, c| acc + c.encoded_len())
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        self.header.write_to(buf)?;
        for c in self.chunks.iter() {
            c.write_to(buf)?;
        }

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < SCTP_HEADER_SIZE {
            return Err("SCTP packet to small".to_owned());
        }
        let header = SctpHeader::new_from_bytes(&mut buf.split_to(SCTP_HEADER_SIZE))?;

        let mut chunks = Vec::new();
        while !buf.is_empty() {
            let chunk: SctpChunk = SctpChunk::new_from_bytes(buf)?;

            // store chunk...
            chunks.push(chunk);
        }

        Ok(Self { header, chunks })
    }
}

pub struct SctpHeader {
    pub source_port: u16,
    pub destination_port: u16,
    pub verification_tag: u32,
    pub checksum: u32,
}

impl SctpHeader {
    pub fn encoded_len(&self) -> usize {
        SCTP_HEADER_SIZE
    }

    pub fn write_to(&self, buf: &mut BytesMut) -> Result<(), String> {
        buf.reserve(self.encoded_len());

        buf.put_u16(self.source_port);
        buf.put_u16(self.destination_port);
        buf.put_u32(self.verification_tag);
        buf.put_u32(self.checksum);

        Ok(())
    }

    pub fn new_from_bytes(buf: &mut Bytes) -> Result<Self, String> {
        if buf.len() < SCTP_HEADER_SIZE {
            Err("sctp header to small".to_owned())
        } else {
            Ok(Self {
                source_port: buf.get_u16(),
                destination_port: buf.get_u16(),
                verification_tag: buf.get_u32(),
                checksum: buf.get_u32(),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chunk::SctpChunkValue;

    const PKT_INIT: [u8; 40] =
        hex!["1388138800000000825d9c050100001a6fdde21800100000ffffffff6f616d178008000682c00000"];
    const PKT_INIT_ACK: [u8; 76] = hex!["13881388b6cbc4387e4dc29c0200003e57d892c600100000ffffffffc362179d0007002479bdd060f5e98ddf84f055807d70d02209f39f75a18571575a6789e6eab861ec8008000682c00000"];
    const PKT_COOKIE_ECHO: [u8; 48] = hex!["13881388fd5ba8fa138638760a000024cf485e0603702b115547ec6810cbe405d4706c90b500507e6b55a89357d1dc0b"];
    const PKT_COOKIE_ACK: [u8; 16] = hex!["1388138880ecb04779d7c4da0b000004"];
    const PKT_DATA: [u8; 36] =
        hex!["13881388fd5ba8fa0a17dea5000700169c7a095a000000000000003370696e6720300000"];
    const PKT_SACK: [u8; 28] = hex!["1388138880ecb047ebea38eb030000109c7a095a0010000000000000"];

    #[test]
    pub fn test_serialize_init1_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_INIT[..]))
            .expect("PKT_INIT must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::Init(_)));
    }

    #[test]
    pub fn test_serialize_init_ack_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_INIT_ACK[..]))
            .expect("PKT_INIT_ACK must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::InitAck(_)));
    }

    #[test]
    pub fn test_serialize_cookie_echo_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_COOKIE_ECHO[..]))
            .expect("PKT_COOKIE_ECHO must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::CookieEcho(_)));
    }

    #[test]
    pub fn test_serialize_cookie_ack_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_COOKIE_ACK[..]))
            .expect("PKT_COOKIE_ACK must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::CookieAck));
    }

    #[test]
    pub fn test_serialize_data_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_DATA[..]))
            .expect("PKT_DATA must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::Data(_)));
    }

    #[test]
    pub fn test_serialize_sack_packet() {
        let pkt = SctpPacket::new_from_bytes(&mut Bytes::copy_from_slice(&PKT_SACK[..]))
            .expect("PKT_SACK must be parsable");

        assert_eq!(pkt.chunks.len(), 1);
        assert!(matches!(pkt.chunks[0].value, SctpChunkValue::SAck(_)));
    }
}
