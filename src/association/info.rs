use std::net::IpAddr;

pub struct SctpAssociationInfo {
    pub port: u16,
    pub outbound_streams: u16,
    pub max_inbound_streams: u16,
    pub a_rwnd: u32,
    pub addresses: Vec<IpAddr>,
}
