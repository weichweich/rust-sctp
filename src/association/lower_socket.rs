use async_trait::async_trait;

#[async_trait]
pub trait LowerSocket {
    type LowerAddress;

    async fn peek_from(&self, buf: &mut [u8]) -> std::io::Result<(usize, Self::LowerAddress)>;

    async fn recv_from(&self, buf: &mut [u8]) -> std::io::Result<(usize, Self::LowerAddress)>;

    async fn send_to(&self, buf: &[u8], dst: Self::LowerAddress) -> std::io::Result<usize>;
}
