pub(crate) fn padding(length: usize, multiple_of: usize) -> usize {
    (multiple_of - length % multiple_of) % multiple_of
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_padding() {
        assert_eq!(padding(0, 4), 0);
        assert_eq!(padding(1, 4), 3);
        assert_eq!(padding(2, 4), 2);
        assert_eq!(padding(3, 4), 1);
        assert_eq!(padding(4, 4), 0);
        assert_eq!(padding(5, 4), 3);
        assert_eq!(padding(6, 4), 2);
        assert_eq!(padding(7, 4), 1);
        assert_eq!(padding(8, 4), 0);
    }
}
