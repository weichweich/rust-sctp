use async_trait::async_trait;
use std::{io, net::SocketAddr};
use tokio::net::{ToSocketAddrs, UdpSocket};

use crate::{
    association::{LowerSocket, SctpAssociationInfo},
    SctpAssociation,
};

pub const MAX_UDP_PACKET_SIZE: usize = 65536;

pub struct SimpleUdpListener {
    sock: UdpSocket,
}

impl SimpleUdpListener {
    pub async fn bind<A: ToSocketAddrs>(addr: A) -> io::Result<Self> {
        let sock = UdpSocket::bind(addr).await?;
        Ok(Self { sock })
    }

    pub async fn accept(self) -> io::Result<SctpAssociation> {
        let local_addr = self.sock.local_addr()?;

        let ls = SimpleUdpLowerSocket { sock: self.sock };

        let info = SctpAssociationInfo {
            port: 5000,
            outbound_streams: 2,
            max_inbound_streams: 2,
            a_rwnd: 1024 * 1024 * 1024,
            addresses: vec![],
        };

        let association = SctpAssociation::accept(local_addr, ls, info)
            .await
            .map_err(|e| {
                log::error!("Error while accepting connection: {:?}", e);
                io::ErrorKind::InvalidData
            })?;

        Ok(association)
    }
}

pub struct SimpleUdpLowerSocket {
    sock: UdpSocket,
}

#[async_trait]
impl LowerSocket for SimpleUdpLowerSocket {
    type LowerAddress = SocketAddr;

    async fn peek_from(&self, buf: &mut [u8]) -> io::Result<(usize, Self::LowerAddress)> {
        self.sock.peek_from(buf).await
    }

    async fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, Self::LowerAddress)> {
        self.sock.recv_from(buf).await
    }

    async fn send_to(&self, buf: &[u8], dst: Self::LowerAddress) -> io::Result<usize> {
        self.sock.send_to(buf, dst).await
    }
}
