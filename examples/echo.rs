use sctp_rust::SimpleUdpListener;
use std::io;

const LISTEN_ADDR: &str = "0.0.0.0:5678";

#[tokio::main]
async fn main() -> io::Result<()> {
    let listener = SimpleUdpListener::bind(LISTEN_ADDR).await?;
    println!("Listening on {}", LISTEN_ADDR);
    let _association = listener.accept().await?;
    println!("Opened SCTP association");

    Ok(())
}
