# SCTP

- toy implementation of [SCTP](https://tools.ietf.org/html/rfc4960).
- Should support the vanilla version and [UDP encapsulation](https://tools.ietf.org/html/rfc6951)

## Questions

- How to encode/decode?
  - impl tokio Encoder/decoder? But they are only required on a high level. Overkill+dependent on tokio (might change, adds maintenance work)
  - custom Encode/Decode trait? Is a trait even necessary? Just add a function?
  - add function? not generic. if we only care about it as encode decode able?
  - use some crate? which crate?
    - Serde is to high level
    - nom is more for languages!? not sure?
    - google protocol buffers. Might be to complicated, adds dependencies, to unrusty, focusses on interop which is given by the RFC?
